from django.db import models
from django.utils import timezone
from datetime import date, time

class Schedules(models.Model):
    nama_kegiatan = models.CharField(max_length=50)
    tanggal_kegiatan = models.DateField()
    waktu_mulai = models.TimeField()
    waktu_selesai = models.TimeField()
    tempat = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50, default='-')
    notes = models.CharField(max_length=200)