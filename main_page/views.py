from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import ScheduleForm
from .models import Schedules
# Create your views here.

def index(request):
	return render(request, 'main/index.html')

def form(request):
	return render(request, 'main/form.html')

def sch_add(request):
	if request.method == "POST":
		schedule = ScheduleForm(request.POST)
		print(schedule.is_valid())
		if schedule.is_valid():
			schedule_data = schedule.cleaned_data
			Schedules.objects.create(**schedule_data)
			print(schedule_data)
			return HttpResponseRedirect('/sch_view')
	else:
		schedule = ScheduleForm()
	content = {'schedule': schedule}
	return render(request, 'main/sch_add.html', content)

def sch_view(request):
	if request.method == "POST":
		Schedules.objects.all().delete()
		return HttpResponseRedirect('/sch_view')
	sch_lst = list(Schedules.objects.all())
	sch_lst.reverse()
	content = {'sch': sch_lst}
	return render(request, 'main/sch_view.html', content)