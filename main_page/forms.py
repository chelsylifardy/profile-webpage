from django import forms


class ScheduleForm(forms.Form):
    nama_kegiatan = forms.CharField(label="Nama Kegiatan", max_length=100, widget=forms.TextInput(attrs={'class':'form-control'}))
    tanggal_kegiatan = forms.DateField(label="Tanggal Kegiatan", widget=forms.DateInput(attrs={'type':'date', 'class':'form-control'}))
    waktu_mulai = forms.TimeField(label="Waktu Mulai", widget=forms.TimeInput(attrs={'type':'time', 'class':'form-control'}))
    waktu_selesai = forms.TimeField(label="Waktu Selesai", widget=forms.TimeInput(attrs={'type':'time', 'class':'form-control'}))
    tempat = forms.CharField(label="Tempat Kegiatan", max_length=100, widget=forms.TextInput(attrs={'class':'form-control'}))
    kategori = forms.CharField(label="Kategori", max_length=100, widget=forms.TextInput(attrs={'class':'form-control'}))
    notes = forms.CharField(label="Notes", widget=forms.Textarea(attrs={'class':'form-control'}), required=False)