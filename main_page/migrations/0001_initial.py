# Generated by Django 2.1.1 on 2018-10-02 12:01

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Schedules',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_kegiatan', models.CharField(max_length=50)),
                ('tanggal_kegiatan', models.DateField()),
                ('waktu_mulai', models.TimeField()),
                ('waktu_selesai', models.TimeField()),
                ('tempat', models.CharField(max_length=50)),
                ('notes', models.CharField(max_length=200)),
            ],
        ),
    ]
